FROM java
ADD ./target/*.jar /saytouma-0.0.1-SNAPSHOT.jar
ADD ./run.sh /run.sh
RUN chmod a+x /run.sh
RUN export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
RUN java -version
EXPOSE 8181:8080
CMD /run.sh